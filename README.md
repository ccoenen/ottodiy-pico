# OTTO DIY Raspberry Pi Pico

With this board, you can use a [Raspberry Pi Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/) board (including the "W" wifi variant) as the heart of an [OTTO DIY](https://www.ottodiy.com/) bot.


![Front of OttoDIY Pico](img/2022-08-05-v2-front.png)
![Back of OttoDIY Pico](img/2022-08-05-v2-back.png)
