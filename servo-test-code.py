import time
import board
import pwmio
from adafruit_motor import servo

# create a PWMOut object on Pin A2.
left_foot_pin = pwmio.PWMOut(board.GP11, duty_cycle=2 ** 15, frequency=50)
left_hip_pin = pwmio.PWMOut(board.GP12, duty_cycle=2 ** 15, frequency=50)
right_hip_pin = pwmio.PWMOut(board.GP13, duty_cycle=2 ** 15, frequency=50)
right_foot_pin = pwmio.PWMOut(board.GP14, duty_cycle=2 ** 15, frequency=50)

# Create a servo object, my_servo.
left_foot = servo.Servo(left_foot_pin)
left_hip = servo.Servo(left_hip_pin)
right_hip = servo.Servo(right_hip_pin)
right_foot = servo.Servo(right_foot_pin)

new_angle = 90;
left_foot.angle = new_angle
left_hip.angle = new_angle
right_hip.angle = new_angle
right_foot.angle = new_angle
time.sleep(0.5)

while True:
    for angle in range(60, 120, 5):  # 0 - 180 degrees, 5 degrees at a time.
        left_foot.angle = angle
        left_hip.angle = angle
        right_hip.angle = angle
        right_foot.angle = angle
        time.sleep(0.05)
    for angle in range(120, 60, -5): # 180 - 0 degrees, 5 degrees at a time.
        left_foot.angle = angle
        left_hip.angle = angle
        right_hip.angle = angle
        right_foot.angle = angle
        time.sleep(0.05)
